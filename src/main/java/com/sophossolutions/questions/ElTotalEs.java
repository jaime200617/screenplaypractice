package com.sophossolutions.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.sophossolutions.userinterfaces.DressesDetail.TOTAL;

public class ElTotalEs implements Question<String> {
    @Override
    public String answeredBy(Actor actor){
        return TOTAL.resolveFor(actor).getText().trim();
    }

    public static ElTotalEs igualA(){
        return new ElTotalEs();
    }
}
