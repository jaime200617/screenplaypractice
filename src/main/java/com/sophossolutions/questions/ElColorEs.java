package com.sophossolutions.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.sophossolutions.userinterfaces.Summary.COLOR;

public class ElColorEs implements Question<String> {

    @Override
    public String answeredBy(Actor actor){
        return COLOR.resolveFor(actor).getText().trim();
    }

    public static ElColorEs igualA(){
        return new ElColorEs();
    }
}
