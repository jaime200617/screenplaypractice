package com.sophossolutions.questions;

import com.sophossolutions.userinterfaces.DressesDetail;
import com.sophossolutions.userinterfaces.Summary;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.sophossolutions.userinterfaces.Summary.TOTAL;

public class TotalFinal implements Question<String> {
    @Override
    public String answeredBy(Actor actor){
        return Summary.TOTAL.resolveFor(actor).getText().trim();
    }

    public static TotalFinal igualA(){
        return new TotalFinal();
    }
}
