package com.sophossolutions.questions;

import com.sophossolutions.userinterfaces.DressesDetail;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;

public class LaCantidadEs implements Question<String> {

    @Override
    public String answeredBy(Actor actor){
        return DressesDetail.QUANTITY.resolveFor(actor).getText().trim();
    }

    public static LaCantidadEs igualA(){
        return new LaCantidadEs();
    }
}
