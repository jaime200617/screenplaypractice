package com.sophossolutions.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.sophossolutions.userinterfaces.Summary.QUANTITY;

public class CantidadFinal implements Question<String> {
    @Override
    public String answeredBy(Actor actor){
        return QUANTITY.resolveFor(actor).getText().trim();
    }

    public static CantidadFinal igualA(){
        return new CantidadFinal();
    }
}
