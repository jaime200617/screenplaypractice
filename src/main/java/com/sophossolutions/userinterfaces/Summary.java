package com.sophossolutions.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class Summary {
    public static final Target TABLE_RESULT = Target.the("Tabla Resultado").locatedBy("//*[@id='cart_summary']");
    public static final Target TOTAL = Target.the("Total Compra").locatedBy("//*[@id='total_price']");
    public static final Target QUANTITY = Target.the("Total Articulos").locatedBy("//*[@id='product_4_43_0_0']/td[5]/input[2]");
    public static final Target COLOR = Target.the("Color Final").locatedBy("//*[@id='product_4_16_0_0']/td[2]/small[2]/a");
}
