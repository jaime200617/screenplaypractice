package com.sophossolutions.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class DressesDetail {

    /*LAYOUT*/
    public static final Target RESULT_DETAIL = Target.the("Resultado Detalle").locatedBy("//*[@id='buy_block']");

    /*BUTTONS*/
    public static final Target ADD_QUANTITY_BUTTON = Target.the("Agregar más").locatedBy("//*[@id='quantity_wanted_p']/a[2]/span/i");
    public static final Target ADD_TO_CART_BUTTON = Target.the("Agregar Carrito").locatedBy("//*[@id='add_to_cart']/button/span");
    public static final Target COLOR_BUTTON = Target.the("Color").locatedBy("//*[@id='color_24']");
    public static final Target PROCEED_BUTTON = Target.the("Total").locatedBy("//*[@id='layer_cart']/div[1]/div[2]/div[4]/a/span");

    /*LABELS*/
    public static final Target QUANTITY = Target.the("Cantidad Actual").locatedBy("//*[@id='layer_cart_product_quantity']");
    public static final Target TOTAL = Target.the("Total").locatedBy("//*[@id='layer_cart']/div[1]/div[2]/div[3]/span");
}
