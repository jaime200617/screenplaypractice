package com.sophossolutions.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class EveningDresses {

    public static final Target RESULT_DRESS = Target.the("Resultado de búsqueda").locatedBy("//*[img[@title='Printed Dress']]");
    public static final Target BUTTON_MORE = Target.the("Botón More").locatedBy("//*[@title='View']");

}
