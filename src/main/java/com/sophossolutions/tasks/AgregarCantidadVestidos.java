package com.sophossolutions.tasks;

import com.sophossolutions.userinterfaces.DressesDetail;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;

public class AgregarCantidadVestidos implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(MoveMouse.to(DressesDetail.RESULT_DETAIL), Click.on(DressesDetail.ADD_QUANTITY_BUTTON));
    }

    public static AgregarCantidadVestidos agregar(){
        return Tasks.instrumented(AgregarCantidadVestidos.class);
    }
}
