package com.sophossolutions.tasks;

import com.sophossolutions.userinterfaces.DressesDetail;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.sophossolutions.userinterfaces.DressesDetail.QUANTITY;
import static com.sophossolutions.userinterfaces.DressesDetail.RESULT_DETAIL;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class AgregarVestidoACarrito implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(MoveMouse.to(RESULT_DETAIL),Click.on(DressesDetail.ADD_TO_CART_BUTTON),
                WaitUntil.the(QUANTITY, isVisible()).forNoMoreThan(2).seconds());
    }

    public static AgregarVestidoACarrito agregar(){
        return Tasks.instrumented(AgregarVestidoACarrito.class);
    }
}
