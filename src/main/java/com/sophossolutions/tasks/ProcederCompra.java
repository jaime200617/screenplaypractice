package com.sophossolutions.tasks;

import com.sophossolutions.userinterfaces.DressesDetail;
import com.sophossolutions.userinterfaces.Summary;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.sophossolutions.userinterfaces.DressesDetail.QUANTITY;
import static com.sophossolutions.userinterfaces.Summary.COLOR;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class ProcederCompra implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                MoveMouse.to(DressesDetail.RESULT_DETAIL), Click.on(DressesDetail.PROCEED_BUTTON)
        );
    }

    public static ProcederCompra procederCompra(){
        return Tasks.instrumented(ProcederCompra.class);
    }
}
