package com.sophossolutions.tasks;

import com.sophossolutions.userinterfaces.DressesDetail;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;

public class SeleccionarColor implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(MoveMouse.to(DressesDetail.RESULT_DETAIL), Click.on(DressesDetail.COLOR_BUTTON));
    }

    public static SeleccionarColor seleccionarColor(){
        return Tasks.instrumented(SeleccionarColor.class);
    }
}
