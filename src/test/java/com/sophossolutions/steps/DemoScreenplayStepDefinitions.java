package com.sophossolutions.steps;

import com.sophossolutions.exceptions.GeneralDemoException;
import com.sophossolutions.questions.*;
import com.sophossolutions.tasks.*;
import com.sophossolutions.userinterfaces.DressesDetail;
import com.sophossolutions.userinterfaces.EveningDresses;
import com.sophossolutions.userinterfaces.HomePageYourLogo;
import com.sophossolutions.userinterfaces.Summary;
import com.sophossolutions.utilities.Constanst;
import com.sophossolutions.utilities.ErrorMessage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.sophossolutions.userinterfaces.DressesDetail.QUANTITY;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.hamcrest.Matchers.equalTo;

public class DemoScreenplayStepDefinitions {

    @Before
    public void setup(){
        setTheStage(new OnlineCast());
    }

    @Given("^Necesito buscar un vestido$")
    public void necesitoBuscarUnVestido() {
        theActorCalled(Constanst.ACTOR_NAME).wasAbleTo(Open.browserOn(new HomePageYourLogo()));
        theActorInTheSpotlight().wasAbleTo(
                BuscarVestido.buscarVestido(),
                VerDetalleVestido.verDetalleVestido()
        );
    }

    @When("^El tipo de vestido es nocturno$")
    public void elTipoDeVestidoEsNocturno() {
        theActorInTheSpotlight().attemptsTo(
                AgregarCantidadVestidos.agregar(),
                SeleccionarColor.seleccionarColor(),
                AgregarVestidoACarrito.agregar()
        );
    }

    @Then("^Valido que el vestido sea agregado al carrito de compras$")
    public void validoQueElVestidoSeaAgregadoAlCarritoDeCompras() {

        theActorInTheSpotlight().should(
                seeThat(LaCantidadEs.igualA(),equalTo(Constanst.QUANTITY)),
                seeThat(ElTotalEs.igualA(),equalTo(Constanst.TOTAL))
        );

        theActorInTheSpotlight().attemptsTo(
                ProcederCompra.procederCompra()
        );

        theActorInTheSpotlight().attemptsTo(
                MoveMouse.to(Summary.TABLE_RESULT)
        );

        theActorInTheSpotlight().should(
                seeThat(TotalFinal.igualA(),equalTo(Constanst.TOTAL))
        );
    }

}
